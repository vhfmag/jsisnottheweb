import * as React from "react";

const hrefPrefix = "./assets/screenshots/";

// TODO: suporte a audio e video

const screenshots = [
	{
		caption: "Modo de leitura do Apple Watch",
		href: "applewatch-article.png",
	},
	{
		caption: "Exibição de <figure> no Apple Watch",
		href: "applewatch-figure.png",
	},
	{
		caption: "Inputs no Apple Watch",
		href: "applewatch-inputs.png",
	},
	{
		caption: "Labels no Apple Watch",
		href: "applewatch-label.png",
	},
	{
		caption: "Dados estruturados no Apple Watch",
		href: "applewatch-microdata.png",
	},
	{
		caption: "Modo de leitura do Firefox",
		href: "firefox-readingmode.png",
	},
	{
		caption: "Listagem de notícias no Google",
		href: "google-notícias.png",
	},
	{
		caption: "Críticas de filme no Google",
		href: "google-reviews.png",
	},
	{
		caption: "Interação rica com o Google Assistant",
		href: "googleassistant-preview.jpg",
	},
	{
		caption: "Informações sobre organização no Google",
		href: "google-organization.png",
	},
	{
		caption: "Preview de receita no Google",
		href: "google-receita.png",
	},
	{
		caption: "Modo de leitura rápida no Telegram",
		href: "telegram-leiturarapida.jpg",
	},
	{
		caption: "Pocket",
		href: "pocket.png",
	},
];

const getSlug = screenshot => screenshot.href.split(".")[0];
const slugMatches = slugs => screenshot => slugs.includes(getSlug(screenshot));

export class Screenshots extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = { currentScreenshot: undefined };
		this.onToggle = screenshot => () => {
			const slug = getSlug(screenshot);
			this.setState({
				currentScreenshot: this.state.currentScreenshot === slug ? undefined : slug,
			});
		};
	}

	render() {
		return (
			<div style={{ textAlign: "left" }}>
				{screenshots.filter(slugMatches(this.props.slugs)).map(screenshot => (
					<details
						key={screenshot.href}
						// open={this.state.currentScreenshot === getSlug(screenshot)}
					>
						<summary>{screenshot.caption}</summary>
						<img src={hrefPrefix + screenshot.href} />
					</details>
				))}
			</div>
		);
	}
}
