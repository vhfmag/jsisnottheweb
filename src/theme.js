import * as React from "react";
import { future as baseTheme } from "mdx-deck/themes";
import okaidia from "react-syntax-highlighter/styles/prism/okaidia";
import MD from "mdx-deck/dist/components";

const highlight = "#fff";

export default {
	...baseTheme,
	font: "Lato, sans-serif",
	css: {
		...baseTheme.css,
		fontSize: "14px",
		"@media screen and (min-width:64em)": {
			fontSize: "26px",
		},
		"h1, h2, h3, h4, h5, h6": {
			maxWidth: "75vw",
		},
	},
	colors: {
		...baseTheme.colors,
		text: "#26d3e0",
		blue: highlight,
		link: highlight,
		pre: highlight,
		code: highlight,
	},
	blockquote: {
		borderLeft: `0.5em solid ${baseTheme.colors.text}`,
		paddingLeft: "1em",
	},
	prism: {
		style: okaidia,
	},
	components: {
		img: props => <MD.img title={props.alt} {...props} />,
	},
};
